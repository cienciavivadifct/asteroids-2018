import sys # We need stuff to interact with the system

from game2d import Game2D
from gameactor import GameActor
from panda3d.core import LPoint2, LVector2

from ship import Ship
from asteroid import Asteroid
from random import choice
from direct.gui.OnscreenText import OnscreenText

class AsteroidsGame(Game2D):
    
    ASTEROIDS_PER_LEVEL = 10
    SAFE_DISTANCE = 50
    
    """
    Class for our AsteroidsGame.
    """
    def __init__(self):
        # We always need to call the __init__() function from the parent class.
        Game2D.__init__(self)

        # Our specific code goes here...
        # Everything that is particular to the asteroids game is here...

        # We need to distinguish several actors
        # We use different "sets" for different kinds of actors
        self.asteroids = []
        self.bullets = []

        # Create the different game actors 
        self.createActors()
        
        # We need to know what is the status of each relevant key
        self.keys = {
            "left":False,  # 0 is NO, 1 is YES
            "right":False, 
            "accel":False,
            "fire":False
        }
        
        # We need to associate the keyboard events to the status of the keys
        self.accept("escape", sys.exit) # Escape key exist the game with function sys.exit
        # And now for the other key events that control the ship's movement
        self.accept("arrow_left",      self.setKey, ["left",True])
        self.accept("arrow_left-up",   self.setKey, ["left",False])
        self.accept("arrow_right",     self.setKey, ["right",True])
        self.accept("arrow_right-up",  self.setKey, ["right",False])
        self.accept("arrow_up",        self.setKey, ["accel",True])
        self.accept("arrow_up-up",     self.setKey, ["accel",False])
        self.accept("space",           self.setKey, ["fire",True])
        self.accept("space-up",        self.setKey, ["fire",False])

        self.loadSounds()

    def loadSounds(self):
        # load the sound files for effects
        self.sndExplosion = loader.loadSfx("sounds/Die.ogg") # Other formats are accepted
        self.sndBlast =     loader.loadSfx("sounds/Lazer.ogg")
        
    def setKey(self, key, value):
        self.keys[key] = value
        
    
    def addBullet(self, bullet):
        self.bullets.append(bullet)
        
        
    def removeBullet(self, bullet):
        self.bullets.remove(bullet)
        
    
    def addAsteroid(self, asteroid):
        self.asteroids.append(asteroid)
        

    def removeAsteroid(self, asteroid):
        self.asteroids.remove(asteroid)
        
        
    def createActors(self):
        self.createBackground()
        self.createShip()
        self.createAsteroids()
        
        
    def createBackground(self):
        # Create an actor that represent the background image
        bg = GameActor("stars.jpg",
                       LPoint2(self.width/2, self.height/2),
                       LVector2(self.width,self.height),
                       10,
                       False)
        
        
    def createShip(self):
        # Create a ship and place it in the center of the screen
        self.ship = Ship(LPoint2(self.width/2,self.height/2))
        self.addActor(self.ship)
        
    
    def createAsteroids(self):
        for i in range(0,AsteroidsGame.ASTEROIDS_PER_LEVEL):
            asteroid = Asteroid(self.randomPosition())
            self.addActor(asteroid)
            self.addAsteroid(asteroid)


    def randomPosition(self):
        x = choice(
            range(0,self.width/2-AsteroidsGame.SAFE_DISTANCE) + 
            range(self.width/2+AsteroidsGame.SAFE_DISTANCE, self.width)
        )
        y = choice(
            range(0,self.height/2-AsteroidsGame.SAFE_DISTANCE) + 
            range(self.height/2+AsteroidsGame.SAFE_DISTANCE, self.height)
        )
        return LPoint2(x,y)
    

    def handleColisions(self):
        # Colisions are an important part of the game

        # An asteroid will kill our ship
        for asteroid in self.asteroids:
            if asteroid.collidesWith(self.ship):
                self.ship.kill()
        
        # A bullet will hit an asteroid
        for bullet in self.bullets:
            for asteroid in self.asteroids:
                if not bullet.expired() and bullet.collidesWith(asteroid):
                    self.sndExplosion.play()
                    bullet.kill()
                    asteroid.kill()

                    self.totalScore += asteroid.getScore()

                    pieces = asteroid.blast()
                    self.removeBullet(bullet)
                    self.removeAsteroid(asteroid)
                    for piece in pieces:
                        self.addActor(piece)
                        self.addAsteroid(piece)


    def showGameOverMessage(self):
        self.message = OnscreenText("GAME OVER", pos=(400,300), parent=camera, fg=(1,0.1,0.1,1), scale = 60)

    def gameLoop(self, dt):

        # Check the keys and decide what to do
        if self.keys["left"]:
            self.ship.rotateLeft(dt)
        elif self.keys["right"]:
            self.ship.rotateRight(dt)
                
        if self.keys["accel"]:
            self.ship.thrust(dt)
        
        if self.keys["fire"]:
            bullet = self.ship.fire()
            self.addActor(bullet)
            self.addBullet(bullet)
            self.keys["fire"] = False
            self.sndBlast.play()
        
        for actor in self.actors:
            self.adjustPosition(actor)

        self.handleColisions()
        
        if self.ship.isDead():
            self.showGameOverMessage()
            return Game2D.done
        else:
            return Game2D.cont
    

    def adjustPosition(self, actor):
        pos = actor.getPosition()
        x = pos.getX() % self.width
        y = pos.getY() % self.height
        actor.setPosition(LPoint2(x,y))


# Let us create an object from our AsteroidsGame class...
game = AsteroidsGame()
# ... and ask it to run
game.run()
