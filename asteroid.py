from gameactor import GameActor
from random import random, choice
from math import sin, cos, pi

from panda3d.core import LVector2

class Asteroid(GameActor):
    
    INITIAL_SPEED = 20
    LARGE_SIZE = 60
    SMALL_SIZE = 10
    SCORE_FACTOR = 2
    INITIAL_SCORE = 10

    def __init__(self, pos):
        GameActor.__init__(self, 
                           choice(["asteroid1.png","asteroid2.png","asteroid3.png"]),
                           pos, 
                           LVector2(60,60),
                           25)
        
        # Starting direction is random
        heading = random() * 2 * pi
        velocity = LVector2(sin(heading), cos(heading)) * Asteroid.INITIAL_SPEED

        # Velocity is constant and fixed.
        self.setVelocity(velocity)

        # The score associated to an asteroid is fixed in the beginning 
        self.score = Asteroid.INITIAL_SCORE


    # The function specific to asteroids to produce the pieces 
    # of its destruction
    def blast(self):

        myVel = self.getVelocity()
        
        if self.getSize().getX() <= Asteroid.SMALL_SIZE:
            return []
        
        a1Vel = LVector2(myVel.getY(),-myVel.getX())        
        a2Vel = LVector2(-myVel.getY(),myVel.getX())
        
        a1 = Asteroid(self.getPosition())
        a1.setVelocity(a1Vel)
        a1.setScore(self.getScore()*Asteroid.SCORE_FACTOR)
        a2 = Asteroid(self.getPosition())
        a2.setVelocity(a2Vel)
        a2.setScore(self.getScore()*Asteroid.SCORE_FACTOR)
                
        mySize = self.getSize()
        
        a1.setSize(mySize - LVector2(10,10))
        a2.setSize(mySize - LVector2(10,10))
                
        return [a1,a2]
