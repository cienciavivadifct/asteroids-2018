from math import sin, cos, pi

from gameactor import GameActor
from panda3d.core import LVector2

from bullet import Bullet

class Ship(GameActor):
    
    # Degrees by unit of time
    ROTATION_SPEED = 200
    THURST_POWER = 100
    MAX_SPEED = 140
    
    def __init__(self, pos):
        GameActor.__init__(self,"ship.png",pos,LVector2(16,16),40)
        # Define the image file, the initial position, the size of 
        # the image, and the depth in the 3D space of the game
        

    def rotate(self, amount):
        # get the current heading (Degrees in relation to the vertical)
        heading = self.getR()
        # change the local value for heading (the resulting value is between 0 and 360)
        heading = (heading + amount) % 360
        # modify the ship's heading
        self.setR(heading)
        
        
    def rotateLeft(self, dt):
        # The angle convention for Panda3D is clockwise from the vertical
        self.rotate(-dt * Ship.ROTATION_SPEED)
        
        
    def rotateRight(self, dt):
        # The angle convention for Panda3D is clockwise from the vertical
        self.rotate(dt * Ship.ROTATION_SPEED)

    
    def thrust(self, dt):
        # Computes the acceleration direction
        adjustedHeading = 90 - self.getR()
        rollInRad = adjustedHeading * pi / 180
        
        # The acceleration vector (in dt) is given by ... 
        dv = LVector2(cos(rollInRad), sin(rollInRad)) * Ship.THURST_POWER * dt
        
        vel = self.getVelocity()
        vel = vel + dv 
        self.setVelocity(vel)
        

    def fire(self):
        adjustedHeading = 90 - self.getR()
        rollInRad = adjustedHeading * pi / 180
        velocityVector = LVector2(cos(rollInRad), sin(rollInRad))
        
        bullet = Bullet(self.getPosition())
        bullet.setVelocity(velocityVector * Bullet.BULLET_SPEED + self.getVelocity())
        
        return bullet
        
        
    def setVelocity(self, velocity):
        speed = velocity.length()
        if speed > Ship.MAX_SPEED:
            velocity.normalize()
            velocity = velocity * Ship.MAX_SPEED
        GameActor.setVelocity(self, velocity)

    