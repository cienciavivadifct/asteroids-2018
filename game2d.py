from direct.showbase.ShowBase import ShowBase
from panda3d.core import OrthographicLens
from direct.task import Task
from direct.gui.OnscreenText import OnscreenText

class Game2D(ShowBase):
    """
    A class for 2D Games in Python.

    This class is a wrapper class around ShowBase class from Panda3D.
    Panda3D is a framework for 3D games and applications. Our 2D games use only 2D (x,y) coordinates.
    However, Panda3D only cares about 3D worlds, so we do the following conversion:

        Game2D horizontal (x) direction is Panda3D horizontal (x) direction
        Game2D vertical (y) direction is Panda3D vertical (z) direction

        Panda3D y direction is not used and points to our eyes.
    """

    # Default width and height for game windows
    DEFAULT_WIDTH = 800
    DEFAULT_HEIGHT = 600

    # Default depth range for visible objects
    NEAR_VAL = 0
    FAR_VAL = 1

    # Constants returned by gameLoop
    cont = Task.cont
    done = Task.done

    def __init__(self, width=DEFAULT_WIDTH, height=DEFAULT_HEIGHT):
        ShowBase.__init__(self)

        # We use an Orthographic projection for 2D
        lens = OrthographicLens()

        # And set the film size to work with pixel units,
        # or whatever is appropriate for your scene
        lens.setFilmSize(width, height)

        # Adjust the offset so that bottom-left corner is at (0,0)
        # and top-right at (SCREEN_WIDTH,SCREEN_HEIGHT)
        lens.setFilmOffset(width/2,height/2)

        # Near and far plane values are adjusted to include Y=0
        lens.setNearFar(Game2D.NEAR_VAL, Game2D.FAR_VAL)

        # Updates the camera node of our scence
        base.cam.node().setLens(lens)

        self.gameTask = taskMgr.add(self.topLevelGameLoop, "gameLoop")

        self.width = width
        self.height = height

        self.actors = []
        
        self.totalScore = 0
        self.createScoreBoard()


    
    def addActor(self, actor):
        self.actors.append(actor)
    
    def createScoreBoard(self):
        self.scoreBoard = OnscreenText(text="Score:0", 
                                       parent=camera, 
                                       pos=(50,570), 
                                       fg=(1,1,1,1), 
                                       scale=18)
        
    def updateScore(self):
        self.scoreBoard.setText("Score: "+str(self.totalScore))
        

    def topLevelGameLoop(self, task):
        # Get the delta time that has elapsed since we were last here
        dt = globalClock.getDt()

        # Update all actors
        
        stillAlive = []
        
        for actor in self.actors:
            if actor.expired():
                actor.removeNode()
            else:
                stillAlive.append(actor)
                actor.update(dt)
                
        self.actors = stillAlive

        self.updateScore()
        
        # Pass the control to the game gameLoop() method and return whatever is returned from gameLoop()
        return self.gameLoop(dt)


    # game loop for a specific game. Derived classes will probably override this to
    # do whatever is needed in a particular game. We simply return Game2D.cont to signal
    # that the game is to continue running
    def gameLoop(self, dt):
        return cont
