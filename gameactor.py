from math import fabs

from panda3d.core import NodePath
from panda3d.core import LPoint2, LVector2
from panda3d.core import LPoint3
from panda3d.core import TransparencyAttrib

class GameActor(NodePath):
    
    IMORTAL = None
    DEAD = 0
    
    def __init__(self, 
                 tex=None, 
                 pos=LPoint2(0,0), 
                 size=LVector2(32,32),
                 order = 10,
                 transparency=True):
        obj = loader.loadModel("models/plane")
        NodePath.__init__(self, obj)
        self.reparentTo(camera)

        # Set the initial position and scale of the actor
        self.setPos(pos.getX(), 0.5, pos.getY())
        self.setScale(size.getX(), 1, size.getY())

        self.setDepthTest(False)

        if transparency:
            # Enable transparency blending
            self.setTransparency(TransparencyAttrib.MAlpha)

        if tex:
            # Load and set the requested texture
            img = loader.loadTexture("textures/" + tex)
            self.setTexture(img, 1)

        self.setBin("background", order)
        
        # velocity attribute for actors, starts stopped
        self.velocity = LVector2(0,0)
        
        self.age = 0
        self.lifetime = GameActor.IMORTAL

        self.score = 0
        

    # sets the current position
    def setPosition(self, pos):
        newPos = LPoint3(pos.getX(), self.getPos().getY(), pos.getY())
        self.setPos(newPos)

    # return the current position
    def getPosition(self):
        pos3d = self.getPos()
        return LPoint2(pos3d.getX(), pos3d.getZ())
    
    def getVelocity(self):
        return self.velocity

    def setVelocity(self, velocity):
        self.velocity = velocity
        
    def setLifetime(self, lt):
        self.lifetime = lt
        
    def expired(self):
        return self.lifetime != GameActor.IMORTAL and self.age > self.lifetime
    
    def kill(self):
        self.lifetime = GameActor.DEAD

    def isDead(self):
        return self.expired()

    def getSize(self):
        return LVector2(self.getScale().getX(), self.getScale().getZ())
    
    def setSize(self, size):
        self.setScale(size.getX(), 1, size.getY())

    def getScore(self):
        return self.score

    def setScore(self, score):
        self.score = score

    def collidesWith(self, other):
        
        myPos = self.getPosition()
        otherPos = other.getPosition()
        
        mySize = self.getSize()
        otherSize = other.getSize()
        
        dx = fabs(myPos.getX() - otherPos.getX())
        if dx >= (mySize.getX() + otherSize.getX())/2:
            return False
        
        dy = fabs(myPos.getY() - otherPos.getY())
        if dy >= (mySize.getY() + otherSize.getY())/2:
            return False
        
        return True


    def update(self, dt):
        # increment the age of the actor
        self.age = self.age + dt
        
        # update position based on velocity
        pos = self.getPosition()
        pos = pos + self.getVelocity() * dt
        self.setPosition(pos)